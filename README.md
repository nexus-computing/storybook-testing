# farmos_proto

Based on Vue2 with Composition API + Vuetify + Storybook
Use of yarn:

## after yarn install, serve project

```
yarn serve
```

## run Storybook

```
yarn storybook
```

---

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
