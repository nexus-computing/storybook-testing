export const parameters = {
  actions: { argTypesRegex: '^on.*' }
};

import { withVuetify } from '@socheatsok78/storybook-addon-vuetify/dist/decorators';

export const decorators = [withVuetify];
