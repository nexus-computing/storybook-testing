import Vue from 'vue';
import App from './App.vue';
import VueCompositionAPI from '@vue/composition-api';
import Vuetify from 'vuetify';
import { addDecorator } from '@storybook/vue';
import 'vuetify/dist/vuetify.min.css';

Vue.use(VueCompositionAPI);
Vue.use(Vuetify);
const vuetifyOptions = {};

addDecorator(() => ({
  template: '<v-app><story/></v-app>'
}));

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  vuetify: new Vuetify(vuetifyOptions)
}).$mount('#app');
