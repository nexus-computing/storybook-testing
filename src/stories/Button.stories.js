import Vue from 'vue';
import VueCompositionApi from '@vue/composition-api';
Vue.use(VueCompositionApi);

import CustomButton from '../components/common/Button.vue';

export default {
  title: 'Button',
  component: CustomButton,
  argTypes: {}
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { CustomButton },
  template: `<div style="padding:10px; background-color:rgb(75, 72, 72)">
    <custom-button @onClick="onClick" v-bind="$props" />
    </div>`
});

export const PerDefault = Template.bind({});
PerDefault.args = {
  label: 'some Button'
};

export const Primary = Template.bind({});
Primary.args = {
  primaryGreen: true,
  label: 'Save and Exit'
};

export const Secondary = Template.bind({});
Secondary.args = {
  secondaryGreen: true,
  label: 'exit without saving'
};

export const WhiteButton = Template.bind({});
WhiteButton.args = {
  colorWhite: true,
  noBorder: true,
  label: 'Profile Settings'
};

export const LittleBlue = Template.bind({});
LittleBlue.args = {
  little: true,
  noBorder: true,
  colorBlue: true,
  label: 'access'
};
