import DemoComponent from "../components/DemoComponent.vue";

export default {
  title: "Demo Component",
  component: DemoComponent,
  argTypes: {
    removeGroup: {},
    removeUser: {},
  },
};


const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { DemoComponent },
  template: `<demo-component  v-bind="$props" />`,
});

export const DemoDefault = Template.bind({});
DemoDefault.args = {
  users: [
    {
      id: 1,
      name: "user 1",
    },
    {
      id: 2,
      name: "user 2",
    },
  ],
  groups: [
    {
      id: 11,
      name: "group 1",
    },
    {
      id: 12,
      name: "group 2",
    },
  ],
};
